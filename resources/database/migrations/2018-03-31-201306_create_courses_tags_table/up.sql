CREATE TABLE courses_tags(
    id            INT            NOT NULL PRIMARY KEY,
    tag           VARCHAR(50)    NOT NULL,
    course_id    INT            NOT NULL REFERENCES courses(id)
)
