CREATE TABLE contact_us_messages(
    id          INT             NOT NULL PRIMARY KEY,
    sent_at     DATETIME        NOT NULL DEFAULT (datetime('now')),
    nickname    VARCHAR(25)     NOT NULL CHECK (LENGTH(nickname) >= 3),
    email       VARCHAR(254)    NOT NULL,
    phone       VARCHAR(14)     CHECK (phone LIKE '05________%'),
    status      INT(2)          NOT NULL DEFAULT 0,
    category    INT(3)          NOT NULL DEFAULT 0,
    subject     VARCHAR(50)     NOT NULL,
    content     VARCHAR(300)    NOT NULL
)
