use errors::*;
use diesel::sqlite::SqliteConnection;
use r2d2_diesel::ConnectionManager;
use r2d2;
use std::ops::Deref;
use rocket::{
    Request,
    State,
    Outcome,
    http::Status,
    request::{self, FromRequest},
};
use self::{
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::ContactUsMessagesDb,
    courses::CoursesDb,
    posts::PostsDb,
    services::ServicesDb,
    users::UsersDb,
};

pub mod types;
pub mod schema;
pub mod users;
pub mod services;
pub mod posts;
pub mod courses;
pub mod clubs;
pub mod contact_us_messages;
pub mod about_us;

pub type Pool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

pub fn init_db_pool() -> Result<Pool> {
    let manager = ConnectionManager::<SqliteConnection>::new("resources/database/db.sqlite");
    r2d2::Pool::new(manager)
        .chain_err(|| "Couldn't create database pool")
}

pub struct DbConn(pub r2d2::PooledConnection<ConnectionManager<SqliteConnection>>);

impl Deref for DbConn {
    type Target = SqliteConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<DbConn, ()> {
        let pool = request.guard::<State<Pool>>()?;
        match pool.get() {
            Ok(conn) => Outcome::Success(DbConn(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ()))
        }
    }
}

pub trait DatabaseConnection {
    fn db_connection(&self) -> &DbConn;
}

impl DatabaseConnection for DbConn {
    fn db_connection(&self) -> &DbConn {
        &self
    }
}

impl AboutUsDb for DbConn { }
impl ClubsDb for DbConn { }
impl ContactUsMessagesDb for DbConn { }
impl CoursesDb for DbConn { }
impl PostsDb for DbConn { }
impl ServicesDb for DbConn { }
impl UsersDb for DbConn { }
