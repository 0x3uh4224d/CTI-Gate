use self::types::*;
use errors::*;
use diesel::prelude::*;
use checked_types::*;
use pages::login::LoginForm;
use pages::Pagination;
use pages::new_member::SignUpForm;
use std::{
    ops::Deref,
    convert::TryFrom,
};
use database::{
    DbConn,
    DatabaseConnection,
    schema::users as users_table,
};
use rocket::{
    Outcome,
    request::{self, FromRequest, Request},
};

pub mod types;

#[derive(Queryable, Insertable, Debug, Clone, Serialize, Deserialize)]
#[table_name = "users_table"]
pub struct User {
    pub id: Id,
    pub username: Username,
    pub page_id: PageId,
    pub first_name: FirstName,
    pub last_name: LastName,
    pub permission: Permission,
    pub status: Status,
    pub phone: Option<Phone>,
    pub email: Email,
    pub password: PasswordHash,
    pub slat: Slat,
    pub registered_at: DateTime,
}

impl User {
    pub fn is_admin(&self) -> bool {
        self.permission == Permission::Admin || self.permission == Permission::Root
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserContext {
    pub username: Username,
    pub page_id: PageId,
    pub first_name: FirstName,
    pub last_name: LastName,
    pub permission: Permission,
    pub status: Status,
    pub phone: Option<Phone>,
    pub email: Email,
    pub registered_at: DateTime,
}

impl From<User> for UserContext {
    fn from(user: User) -> Self {
        Self {
            username: user.username,
            page_id: user.page_id,
            first_name: user.first_name,
            last_name: user.last_name,
            permission: user.permission,
            status: user.status,
            phone: user.phone,
            email: user.email,
            registered_at: user.registered_at,
        }
    }
}

// Make User a request guard
impl<'a, 'r> FromRequest<'a, 'r> for User {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let mut cookies = request.cookies();

        // get username cookie, otherwise forward the request to the next route
        let username = match cookies.get_private("username") {
            Some(cookie) => match Username::try_from(cookie.value()) {
                Ok(username) => username,
                Err(_) => return Outcome::Forward(()),
            },
            None => return Outcome::Forward(()),
        };

        // get password cookie, otherwise forward the request to the next route
        let password = match cookies.get_private("password") {
            Some(cookie) => match PasswordHash::try_from(cookie.value()) {
                Ok(password) => password,
                Err(_) => return Outcome::Forward(()),
            },
            None => return Outcome::Forward(()),
        };

        // get database connection
        let db_conn = DbConn::from_request(request)?;
        // get User from from username and password
        match db_conn.verify_username_and_passwordhash(&username, &password) {
            Ok(user) => Outcome::Success(user),
            Err(_) => Outcome::Forward(()),
        }
    }
}

// TODO: implment these methods
pub trait UsersDb: DatabaseConnection {
    ///
    fn get_users_for_current_page(&self) -> Result<Vec<UserContext>>
    where
        Self: Pagination,
    {
        use database::schema::users::dsl::*;

        let page = self.page();
        self.is_vaild_page_number(page)?;

        let items_per_page = self.items_per_page();
        let conn = self.db_connection();
        let offset = items_per_page * (page as i32 - 1).abs() as u32;

        let users_context = users.order(registered_at.desc())
            .limit(items_per_page as i64)
            .offset(offset as i64)
            .load::<User>(conn.deref())
            .chain_err(|| "Couldn't get users from db")?
            .into_iter()
            .map(|user_data| user_data.into())
            .collect();

        Ok(users_context)
    }

    ///
    fn get_all_users_number(&self) -> Result<u32> {
        use database::schema::users::dsl::*;

        let users_number = users.count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get users number from db")?
            .pop();

        match users_number {
            None => Ok(0),
            Some(number) => Ok(number as u32),
        }
    }

    ///
    fn get_users_number(&self) -> Result<u32> {
        use database::schema::users::dsl::*;

        let users_number = users.filter(status.eq(Status::Fine))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get users number from db")?
            .pop();

        match users_number {
            None => Ok(0),
            Some(number) => Ok(number as u32),
        }
    }

    /// get max_id in users table
    fn get_max_id(&self) -> Result<Option<Id>> {
        use database::schema::users::dsl::*;
        use diesel::dsl::max;

        let max_id = users.order(id)
            .select(max(id))
            .first::<Option<Id>>(self.db_connection().deref())?;

        Ok(max_id)
    }

    fn check_username_is_available(&self, value: &Username) -> Result<()> {
        use database::schema::users::dsl::*;

        let is_used = users.filter(username.eq(value))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't check if username is used or not")?
            .pop();

        match is_used {
            None | Some(0) => Ok(()),
            Some(_) => Err(ErrorKind::NotAvailableUsername(value.to_string()))?,
        }
    }

    fn check_page_id_is_available(&self, value: &PageId) -> Result<()> {
        use database::schema::users::dsl::*;

        let is_used = users.filter(page_id.eq(value))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't check if page id is used or not")?
            .pop();

        match is_used {
            None | Some(0) => Ok(()),
            Some(_) => Err(ErrorKind::NotAvailablePageId(value.to_string()))?,
        }
    }

    fn check_phone_is_available(&self, value: &Phone) -> Result<()> {
        use database::schema::users::dsl::*;

        // TODO: make sure this work
        let is_used = users.filter(phone.eq(Some(value)))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't check if phone is used or not")?
            .pop();

        match is_used {
            None | Some(0) => Ok(()),
            Some(_) => Err(ErrorKind::NotAvailablePhone(value.to_string()))?,
        }
    }

    fn check_email_is_available(&self, value: &Email) -> Result<()> {
        use database::schema::users::dsl::*;

        let is_used = users.filter(email.eq(value))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't check if email is used or not")?
            .pop();

        match is_used {
            None | Some(0) => Ok(()),
            Some(_) => Err(ErrorKind::NotAvailableEmail(value.to_string()))?,
        }
    }

    /// check if there is such a user
    fn get_user_by_page_id(&self, page_id: &PageId) -> Result<User> {
        use database::schema::users;

        let user = users::table
            .filter(users::page_id.eq(page_id))
            .first::<User>(self.db_connection().deref())
            .chain_err(|| "Couldn't fetch users table to get user data by page_id")?;

        Ok(user)
    }

    ///
    fn get_user_by_username(&self, username: &Username) -> Result<User> {
        use database::schema::users;

        let user = users::table
            .filter(users::username.eq(username))
            .first::<User>(self.db_connection().deref())
            .chain_err(|| "Couldn't fetch users table to get user data by username")?;

        Ok(user)
    }

    fn get_slat_by_username(&self, username: &Username) -> Result<Slat> {
        use database::schema::users;

        let slat = users::table
            .filter(users::username.eq(username))
            .select(users::slat)
            .first::<Slat>(self.db_connection().deref())
            .chain_err(|| "Couldn't get slat by useranme")?;

        Ok(slat)
    }

    /// verify `LoginForm` and return `User` if the input are valid
    fn verify_login_form(&self, login: &LoginForm) -> Result<User> {
        // extract info from login form
        let username = LoginForm::ok_or_incomplete_form(&login.username)?;
        let password = LoginForm::ok_or_incomplete_form(&login.password)?;

        // get the slat for the username
        let slat = self.get_slat_by_username(&username)?;
        // get the hashed password from the plain password and slat
        let password = PasswordHash::new(&password, &slat)?;

        // call verify_username_and_passwordhash to do the rest
        self.verify_username_and_passwordhash(&username, &password)
    }


    /// verify `Username` and `PasswordHash` and return `User` if the
    /// input are valid
    fn verify_username_and_passwordhash(
        &self,
        username: &Username,
        password: &PasswordHash
    ) -> Result<User> {
        use database::schema::users;

        // get User from the database where username = username and
        // password = password, if parameters are correct then User will be
        // returned otherwise return Error such as NotFound
        let user = users::table
            .filter(users::username.eq(username))
            .filter(users::password.eq(password))
            .first::<User>(self.db_connection().deref())
            .chain_err(|| "Couldn't load user data to verify username and password")?;

        // return User data
        Ok(user)
    }

    // add new user to the database from SignUpForm
    fn add_new_user(&self, sign_up: &SignUpForm) -> Result<()> {
        use diesel::{self, select};

        // extract data from sign_up form
        let username = SignUpForm::ok_or_incomplete_form(&sign_up.username)?;
        let first_name = SignUpForm::ok_or_incomplete_form(&sign_up.first_name)?;
        let last_name = SignUpForm::ok_or_incomplete_form(&sign_up.last_name)?;
        let phone = match &sign_up.phone {
            None => None,
            Some(phone) => Some(SignUpForm::ok_or_incomplete_form(phone)?),
        };
        let email = SignUpForm::ok_or_incomplete_form(&sign_up.email)?;
        let plain_password = SignUpForm::ok_or_incomplete_form(&sign_up.password)?;

        // check if data are ready to be stored in the database
        self.check_username_is_available(&username)?;
        self.check_email_is_available(&email)?;
        if let Some(ref phone) = phone {
            self.check_phone_is_available(phone)?;
        }

        // generate page_id form username
        let page_id = PageId::new_from_username(&username)?;
        // check if page_id ready to be stored in the database
        self.check_page_id_is_available(&page_id)?;

        // get the max id for the database, then make a new id for the new user
        let id = match self.get_max_id()? {
            Some(value) => value.checked_add(1)?,
            None => Id::try_from(0)?,
        };

        // give the new user default status
        let status: Status = Default::default();
        // give the new user default permission
        let permission: Permission = Default::default();
        // get the current datetime form the database backend
        let registered_at = select(diesel::dsl::now)
            .first::<DateTime>(self.db_connection().deref())?;

        // create random slat for this user
        let slat = Slat::random_slat()?;
        // generate the hashed password using slat we created and the plain password
        // that user enter in the SignUpForm
        let password = PasswordHash::new(&plain_password, &slat)?;

        // fill data into insertable struct, to insert it into the database
        let user_data = User {
            id,
            username,
            page_id,
            first_name,
            last_name,
            permission,
            status,
            phone,
            email,
            password,
            slat,
            registered_at,
        };

        // insert the data after making sure that all we got are valid
        {
            use diesel::insert_into;
            use database::schema::users::dsl::*;
            insert_into(users)
                .values(user_data)
                .execute(self.db_connection().deref())
                .chain_err(|| "Couldn't add a new user")?;
        }

        // return Ok(()) if every thing goes without errors
        Ok(())
    }

    ///
    fn delete_user(&self, pid: &PageId) -> Result<()> {
        use diesel::delete;
        use database::schema::users::dsl::*;

        delete(users.filter(page_id.eq(pid)))
            .execute(self.db_connection().deref())
            .chain_err(|| "Couldn't delete user from db")?;

        Ok(())
    }
}
