use errors::*;
use checked_types::CheckedType;
use std::{
    fmt,
    io,
    convert::TryFrom,
    ops::Deref,
};
use diesel::{
    backend::Backend,
    serialize::{self, ToSql, Output},
    deserialize::{self, FromSql},
    sql_types,
};

// display_impl
pub macro display_impl($type:ident($inner:ident)) {
    impl fmt::Display for $type
    where
        $type: CheckedType<$inner>,
        $inner: fmt::Display,
    {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            self.value().fmt(f)
        }
    }
}

// deref for tuple structs
pub macro deref_impl($type:ident($inner:ident)) {
    impl Deref for $type
    where
        $type: CheckedType<$inner>,
    {
        type Target = $inner;

        fn deref(&self) -> &Self::Target {
            self.value()
        }
    }
}

// helper macro from_sql_impl
pub macro from_sql_impl($sql_type:ident -> $type:ident($inner:ident)) {
    impl<DB: Backend> FromSql<sql_types::$sql_type, DB> for $type
    where
        $inner: FromSql<sql_types::$sql_type, DB>,
        $type: TryFrom<$inner>,
    {
        fn from_sql(bytes: Option<&DB::RawValue>) -> deserialize::Result<Self> {
            let value = $inner::from_sql(bytes)?;
            let value = match $type::try_from(value) {
                Ok(value) => value,
                Err(err) => return Err(format!("Database contains corrupt data!, details: {}", err).into()),
            };
            Ok(value)
        }
    }
}

// helper macro to_sql_impl_for_enum
pub macro to_sql_impl_for_enum($type:ident as $other:ident -> $sql_type:ident) {
    impl<DB: Backend> ToSql<sql_types::$sql_type, DB> for $type
    where
        $other: ToSql<sql_types::$sql_type, DB>,
        $other: From<$type>
    {
        fn to_sql<W>(&self, out: &mut Output<W, DB>) -> serialize::Result
        where
            W: io::Write,
        {
            let value: $other = $other::from(*self);
            value.to_sql(out)
        }
    }
}

// helper macro to_sql_impl
pub macro to_sql_impl($type:ident($inner:ident) -> $sql_type:ident) {
    impl<DB: Backend> ToSql<sql_types::$sql_type, DB> for $type
    where
        $inner: ToSql<sql_types::$sql_type, DB>,
        $type: CheckedType<$inner>,
    {
        fn to_sql<W>(&self, out: &mut Output<W, DB>) -> serialize::Result
        where
            W: io::Write,
        {
            self.value().to_sql(out)
        }
    }
}

// helper macro to impl TryFrom for types who have Self::new() -> Result<Self>
// function
pub macro try_from_impl {
    ($($source:ident)+ -> $target:ident($inner:ident)) => {
        $(
            impl TryFrom<$source> for $target {
                type Error = Error;

                fn try_from(value: $source) -> Result<Self> {
                    let value = value as $inner;
                    $target::check(&value)?;
                    Ok($target(value))
                }
            }
        )+
    }
}

// macro helper to impl TryFrom<String>, TryFrom<&String>, TryFrom<&str>
pub macro try_from_strings($type:ident) {
    impl TryFrom<String> for $type {
        type Error = Error;

        fn try_from(value: String) -> Result<Self> {
            $type::check(&value)?;
            Ok($type(value))
        }
    }

    impl<'a> TryFrom<&'a String> for $type {
        type Error = Error;

        fn try_from(value: &'a String) -> Result<Self> {
            let value = value.clone();
            $type::check(&value)?;
            Ok($type(value))
        }
    }

    impl<'a> TryFrom<&'a str> for $type {
        type Error = Error;

        fn try_from(value: &'a str) -> Result<Self> {
            let value = value.to_string();
            $type::check(&value)?;
            Ok($type(value))
        }
    }
}

// helper macro to impl from for tuple structs
pub macro from_impl {
    ($type:ident($inner:ident) -> Strings) => {
        impl From<$type> for String
        where
            $type: CheckedType<$inner>,
        {
            fn from(other: $type) -> Self {
                other.value().clone()
            }
        }

        impl<'a> From<&'a $type> for String
        where
            $type: CheckedType<$inner>,
        {
            fn from(other: &'a $type) -> Self {
                (*other).value().clone()
            }
        }
    },

    ($source:ident($inner:ident) -> $($target:ident)+) => {
        $(
            impl From<$source> for $target
            where
                $source: CheckedType<$inner>,
            {
                fn from(other: $source) -> Self {
                    *other.value() as $target
                }
            }

            impl<'a> From<&'a $source> for $target
            where
                $source: CheckedType<$inner>,
            {
                fn from(other: &'a $source) -> Self {
                    *other.value() as $target
                }
            }
        )+
    }
}
