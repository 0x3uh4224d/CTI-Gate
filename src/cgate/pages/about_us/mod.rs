use rocket_contrib::Template;
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    users::{User, UserContext},
    about_us::{AboutUsContext, AboutUsDb},
};
use rocket::{
    Route,
    request::Request,
    response::{self, Responder},
    http::Status,
};

#[derive(Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    infos: Vec<AboutUsContext>,
    user: Option<UserContext>,
}

pub struct AboutUsPage {
    conn: DbConn,
    user: Option<User>,
}

impl<'r> AboutUsPage {
    fn new(conn: DbConn, user: Option<User>) -> Self {
        Self {
            conn,
            user,
        }
    }

    fn get_context(&mut self) -> PageContext {
        let mut links = super::get_site_links();
        links[4].to_active();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            infos: vec![],
            user: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();
        let infos = self.get_about_us_context()
            .map_err(|_| Status::InternalServerError)?;

        // user
        let user_context = self.user.clone().map(|user| user.into());

        context.infos = infos;
        context.user = user_context;

        let template = Template::render("about_us/index", context);
        template.respond_to(req)
    }
}

impl<'r> Responder<'r> for AboutUsPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl AboutUsDb for AboutUsPage { }

impl DatabaseConnection for AboutUsPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn about_us_routes() -> Vec<Route> {
    routes![about_us]
}

#[get("/about-us")]
pub fn about_us(conn: DbConn, user: Option<User>) -> AboutUsPage {
    AboutUsPage::new(conn, user)
}
