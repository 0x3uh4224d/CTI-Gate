use errors::*;
use rocket_contrib::Template;
use pages::get_site_links;
use rocket::{
    Route,
    http::Status,
    request::{Request, Form},
    response::{self, Redirect, Responder},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::ContactUsMessagesDb,
    courses::CoursesDb,
    types::{Title, Summary, PageContent},
    posts::{PostContext, PostsDb},
    services::ServicesDb,
    users::{
        User,
        UserContext,
        UsersDb,
        types::{Username, FirstName, LastName, Phone, Email, Password},
    },
};

#[derive(Debug, FromForm)]
pub struct NewPostForm {
    pub title: Result<Title>,
    pub summary: Result<Summary>,
    pub page_content: Result<PageContent>,
    pub draft: bool,
}

impl NewPostForm {
    pub fn ok_or_incomplete_form<T: Clone>(value: &Result<T>) -> Result<T> {
        match value {
            Ok(ref value) => Ok(value.clone()),
            Err(ref err) => {
                let reason = format!("{}", err);
                Err(ErrorKind::IncompleteNewPostForm(reason))?
            },
        }
    }
}

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    user: UserContext,
    show_success_message: bool,
    other_err: Option<String>,
    title_err: Option<String>,
    summary_err: Option<String>,
    page_content_err: Option<String>,
}

impl PageContext {
    pub fn is_new_post_form_invalid(&self) -> bool {
        self.title_err.is_some() || self.summary_err.is_some() ||
            self.page_content_err.is_some()
    }

    pub fn find_new_post_from_errors(&mut self, new_post: &NewPostForm) {
        let to_str = |err: &Error| -> Option<String> {
            Some(format!("{}", err))
        };

        if let Err(ref err) = new_post.title { self.title_err = to_str(err) }
        if let Err(ref err) = new_post.summary { self.summary_err = to_str(err) }
        if let Err(ref err) =
            new_post.page_content { self.page_content_err = to_str(err) }
    }
}

pub struct DNewsAddPage {
    conn: DbConn,
    admin: User,
    post: Option<NewPostForm>,
}

impl<'r> DNewsAddPage {
    fn new(conn: DbConn, admin: User, post: Option<NewPostForm>) -> Self {
        Self {
            conn,
            admin,
            post,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let links = get_site_links();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            user: self.admin.clone().into(),
            show_success_message: false,
            other_err: None,
            title_err: None,
            summary_err: None,
            page_content_err: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let context = self.get_context();
        Template::render("dashboard/news-add", context).respond_to(req)
    }

    fn add_post_handler(&mut self, req: &Request) -> response::Result<'r> {
        if self.post.is_none() {
            return Redirect::to("/dashboard/news/add").respond_to(req);
        }

        let mut context = self.get_context();
        let post_form = self.post.as_ref().unwrap();

        context.find_new_post_from_errors(post_form);
        if context.is_new_post_form_invalid() {
            return Template::render("dashboard/news-add", context).respond_to(req);
        }

        match self.add_new_post(post_form, &self.admin.id) {
            Ok(_) => context.show_success_message = true,
            _ => context.other_err = Some("حصلة مشكلة أثناء إنشاء الخبر".to_string()),
        };

        Template::render("dashboard/news-add", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for DNewsAddPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }

        if self.post.is_none() {
            return self.index_handler(req);
        }
        self.add_post_handler(req)
    }
}

impl UsersDb for DNewsAddPage { }
impl PostsDb for DNewsAddPage { }

impl DatabaseConnection for DNewsAddPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn d_news_add_routes() -> Vec<Route> {
    routes![d_news_add, d_news_add_post]
}

#[get("/dashboard/news/add")]
pub fn d_news_add(conn: DbConn, user: User) -> DNewsAddPage {
    DNewsAddPage::new(conn, user, None)
}

#[post("/dashboard/news/add", data = "<post>")]
pub fn d_news_add_post(
    conn: DbConn,
    admin: User,
    post: Option<Form<NewPostForm>>
) -> DNewsAddPage {
    let post = post.map(|pst| pst.into_inner());
    DNewsAddPage::new(conn, admin, post)
}
