use errors::*;
use rocket_contrib::Template;
use pages::get_site_links;
use rocket::{
    Route,
    http::Status,
    request::{Request, Form},
    response::{self, Redirect, Responder},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::ContactUsMessagesDb,
    courses::CoursesDb,
    types::{Title, Summary, PageContent},
    posts::{PostContext, PostsDb},
    services::ServicesDb,
    users::{
        User,
        UserContext,
        UsersDb,
        types::{Username, FirstName, LastName, Phone, Email, Password, PageId},
    },
};

#[derive(Debug, FromForm)]
pub struct EditPostForm {
    pub title: Result<Title>,
    pub summary: Result<Summary>,
    pub page_content: Result<PageContent>,
    pub draft: bool,
}

impl EditPostForm {
    pub fn ok_or_incomplete_form<T: Clone>(value: &Result<T>) -> Result<T> {
        match value {
            Ok(ref value) => Ok(value.clone()),
            Err(ref err) => {
                let reason = format!("{}", err);
                Err(ErrorKind::IncompleteEditPostForm(reason))?
            },
        }
    }
}

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    user: UserContext,
    show_success_message: bool,
    other_err: Option<String>,
    title_err: Option<String>,
    summary_err: Option<String>,
    page_content_err: Option<String>,
    post: Option<PostContext>
}

impl PageContext {
    pub fn is_edit_post_form_invalid(&self) -> bool {
        self.title_err.is_some() || self.summary_err.is_some() ||
            self.page_content_err.is_some()
    }

    pub fn find_edit_post_from_errors(&mut self, edit_post: &EditPostForm) {
        let to_str = |err: &Error| -> Option<String> {
            Some(format!("{}", err))
        };

        if let Err(ref err) = edit_post.title { self.title_err = to_str(err) }
        if let Err(ref err) = edit_post.summary { self.summary_err = to_str(err) }
        if let Err(ref err) =
            edit_post.page_content { self.page_content_err = to_str(err) }
    }
}

pub struct DNewsEditPage {
    conn: DbConn,
    admin: User,
    post: Option<EditPostForm>,
    page_id: PageId,
}

impl<'r> DNewsEditPage {
    fn new(conn: DbConn, admin: User, page_id: PageId, post: Option<EditPostForm>) -> Self {
        Self {
            conn,
            admin,
            post,
            page_id,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let links = get_site_links();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            user: self.admin.clone().into(),
            show_success_message: false,
            other_err: None,
            title_err: None,
            summary_err: None,
            page_content_err: None,
            post: None,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();

        context.post = Some(
            self.get_post_context_by_page_id(&self.page_id)
                .map_err(|_| Status::NotFound)?
        );

        Template::render("dashboard/news-edit", context).respond_to(req)
    }

    fn edit_post_handler(&mut self, req: &Request) -> response::Result<'r> {
        if self.post.is_none() {
            return Redirect::to(&format!("/dashboard/news/edit/{}", self.page_id)).respond_to(req);
        }

        let mut context = self.get_context();
        let post_form = self.post.as_ref().unwrap();

        context.find_edit_post_from_errors(post_form);
        if context.is_edit_post_form_invalid() {
            return Template::render("dashboard/news-edit", context).respond_to(req);
        }

        match self.update_post(&self.page_id, post_form) {
            Ok(_) => {
                context.show_success_message = true;
                context.post = Some(
                    self.get_post_context_by_page_id(&self.page_id)
                        .map_err(|_| Status::NotFound)?
                );
            },
            Err(ref err) => {
                context.other_err = Some(format!("{}", err));
                context.post = Some(
                    self.get_post_context_by_page_id(&self.page_id)
                        .map_err(|_| Status::NotFound)?
                );
            },
        };

        Template::render("dashboard/news-edit", context).respond_to(req)
    }
}

impl<'r> Responder<'r> for DNewsEditPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }

        if self.post.is_none() {
            return self.index_handler(req);
        }
        self.edit_post_handler(req)
    }
}

impl UsersDb for DNewsEditPage { }
impl PostsDb for DNewsEditPage { }

impl DatabaseConnection for DNewsEditPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn d_news_edit_routes() -> Vec<Route> {
    routes![d_news_edit, d_news_edit_post]
}

#[get("/dashboard/news/edit/<page_id>")]
pub fn d_news_edit(conn: DbConn, user: User, page_id: PageId) -> DNewsEditPage {
    DNewsEditPage::new(conn, user, page_id, None)
}

#[post("/dashboard/news/edit/<page_id>", data = "<post>")]
pub fn d_news_edit_post(
    conn: DbConn,
    admin: User,
    post: Option<Form<EditPostForm>>,
    page_id: PageId,
) -> DNewsEditPage {
    let post = post.map(|pst| pst.into_inner());
    DNewsEditPage::new(conn, admin, page_id, post)
}
