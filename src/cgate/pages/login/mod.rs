use errors::*;
use contexts::Link;
use rocket_contrib::Template;
use database::{
    DbConn,
    DatabaseConnection,
    users::{
        UsersDb,
        User,
        types::{Username, Password},
    },
};
use rocket::{
    Route,
    request::Request,
    response::{self, Redirect, Responder},
    http::{Cookies, Cookie},
    request::Form,
};

#[derive(FromForm, Debug)]
pub struct LoginForm {
    pub username: Result<Username>,
    pub password: Result<Password>,
}

impl LoginForm {
    pub fn ok_or_incomplete_form<T: Clone>(value: &Result<T>) -> Result<T> {
        match value {
            Ok(ref value) => Ok(value.clone()),
            Err(ref err) => {
                let reason = format!("{}", err);
                Err(ErrorKind::IncompleteLoginForm(reason))?
            },
        }
    }
}

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    error_message: Option<String>,
}

pub struct LoginPage<'c> {
    conn: DbConn,
    login_form: Option<LoginForm>,
    cookies: Option<Cookies<'c>>,
}

impl<'c, 'r> LoginPage<'c> {
    fn new(
        conn: DbConn,
        login_form: Option<LoginForm>,
        cookies: Option<Cookies<'c>>
    ) -> Self {
        Self {
            conn,
            login_form,
            cookies,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let links = super::get_site_links();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        let context = PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            error_message: None,
        };

        context
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let context = self.get_context();
        let template = Template::render("login/index", context);
        template.respond_to(req)
    }

    fn login_handler(&mut self, req: &Request) -> response::Result<'r> {
        if self.login_form.is_none() || self.cookies.is_none() {
            return Redirect::to("/login").respond_to(req);
        }

        let mut context = self.get_context();
        let error_message = Some("خطأ في اسم المستخدم أو كلمة المرور".into());

        let login_from = self.login_form.as_ref();
        // if there is a login form then handle it, otherwise
        if let Some(login) = login_from {
            // check if there was any error with the input fields, return error if so.
            if login.username.is_err() || login.password.is_err() {
                context.error_message = error_message;
            } else {
                // verify username and password, and login the user if the input was
                // correct, otherwise return error.
                match self.verify_login_form(login) {
                    Ok(user) => {
                        // get cookies out of user
                        let username_cookie = Cookie::from(user.username.clone());
                        let password_cookie = Cookie::from(user.password.clone());
                        // calling unwarp is ok since we check for none in the start
                        // of the method
                        let mut cookies = self.cookies.as_mut().unwrap();
                        cookies.add_private(username_cookie);
                        cookies.add_private(password_cookie);
                        return Redirect::to("/").respond_to(req);
                    },
                    Err(_) => {
                        context.error_message = error_message;
                    },
                };
            }
        }

        let template = Template::render("login/index", context);
        template.respond_to(req)
    }
}

impl<'r, 'c> Responder<'r> for LoginPage<'c> {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if self.login_form.is_none() {
            return self.index_handler(req)
        }
        self.login_handler(req)
    }
}

impl<'c> UsersDb for LoginPage<'c> { }

impl<'c> DatabaseConnection for LoginPage<'c> {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn login_routes() -> Vec<Route> {
    routes![
        login_while_loged_in,
        login,
        login_form_while_loged_in,
        login_form
    ]
}

#[get("/login")]
pub fn login_while_loged_in(_u: User) -> Redirect {
    Redirect::to("/")
}

#[get("/login", rank = 2)]
pub fn login<'c>(conn: DbConn) -> LoginPage<'c> {
    LoginPage::new(conn, None, None)
}

#[post("/login", data = "<_m>")]
pub fn login_form_while_loged_in(_u: User, _m: Option<Form<LoginForm>>) -> Redirect {
    Redirect::to("/")
}

#[post("/login", data = "<message>", rank =2)]
pub fn login_form<'c>(
    conn: DbConn,
    cookies: Cookies<'c>,
    message: Option<Form<LoginForm>>
) -> LoginPage<'c> {
    let message = message.map(|msg| msg.into_inner());
    LoginPage::new(conn, message, Some(cookies))
}
